window.map = null;
async function initMap() {
    await ymaps3.ready;

    const {YMap, YMapDefaultSchemeLayer, YMapDefaultFeaturesLayer, YMapControls} = ymaps3;
    const {YMapZoomControl} = await ymaps3.import('@yandex/ymaps3-controls@0.0.1');
    const {YMapDefaultMarker} = await ymaps3.import('@yandex/ymaps3-markers@0.0.1');


    const map = new YMap(document.querySelector('#YMapsID'), {
            location: {
                center: [71.427581, 51.129795],
                zoom: 15,
            },
            behaviors: ["drag", "pinchZoom", "dblClick", "multiTouch"],
        }
    );
    map.addChild(new YMapControls({position: 'left'}).addChild(new YMapZoomControl({})));
    map.addChild(new YMapDefaultSchemeLayer());
    map.addChild(new YMapDefaultFeaturesLayer());

    const marker = new YMapDefaultMarker({
        coordinates: [71.427581, 51.129795],
        draggable: false,
        title: 'FlowLingo',
    });
    map.addChild(marker);

}
initMap();
