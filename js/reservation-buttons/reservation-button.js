const modalReservation = document.querySelector("#modal-reservation");
const reservationButtons = document.querySelectorAll(".reservation");
const closeModalReservationButton = document.querySelector("#close-modal-reservation-button");
const signUpButtonReservation = document.querySelector("#button-sign-up");


reservationButtons.forEach(button => {
    button.addEventListener("click", function() {
        openModalReservation();
    });
});



function modalReservationOnClickOutside(event) {
    if (event.target === modalReservation || event.target === closeModalReservationButton) {
        closeModalReservation();
    }

    if (event.target === signUpButtonReservation) {
        event.preventDefault();
        closeModalReservation();
        window.location.href = "#sign-up-link"
    }
}

function openModalReservation(event) {
    modalReservation.showModal();
    window.addEventListener('click', modalReservationOnClickOutside);
}
function closeModalReservation() {
    modalReservation.close();
    window.removeEventListener('click', modalReservationOnClickOutside);
}
