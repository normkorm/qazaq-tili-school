const swiper1 = new Swiper('.swiper1', {
    direction: 'horizontal',
    slidesPerView: 1,
    spaceBetween: 30,
    centeredSlides: true,
    initialSlide: 2,
    loop: true,
    navigation: {
        nextEl: ".swipe-right",
        prevEl: ".swipe-left",
    },

    breakpoints: {
        500: {
            slidesPerView: 1.5,
        },
        800: {
            slidesPerView: 2,
        },
        1400: {
            slidesPerView: 3.5,
        },
    }
});

const swiper2 = new Swiper('.logos-container', {
    loop: true,
    autoplay: {
        delay: 0,
        pauseOnMouseEnter: true,
        disableOnInteraction: false,
    },
    speed: 1600,
    breakpoints: {
        425: {
            slidesPerView: 2,
        },
        768: {
            slidesPerView: 3,
        },
        1024: {
            slidesPerView: 4,
        },
        1440: {
            slidesPerView: 5,
        }
    }

})

const swiper3 = new Swiper('.gallery-wrapper', {
    direction: 'horizontal',
    slidesPerView: 1,
    spaceBetween: 30,
    centeredSlides: true,
    initialSlide: 2,
    loop: true,
    freeMode: false,
    centerInsufficientSlides:true,
    navigation: {
        nextEl: ".swiper-button-next",
        prevEl: ".swiper-button-prev",
    },
    breakpoints: {
        768: {
            slidesPerView: 2.5,
        },
    }
})

const swiper4 = new Swiper(".first-page-carousel-container", {
    direction: 'horizontal',
    slidesPerView: 1,
    loop: true,
    navigation: {
        nextEl: "#fist-page-swiper-button-right",
        prevEl: "#fist-page-swiper-button-left"
    },
    autoplay: {
        delay: 5000
    }
})